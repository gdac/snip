tool
class_name NPC
extends StaticBody
# Non-player character.


onready var animated_sprite: AnimatedSprite3D = $AnimatedSprite3D


func _process(_delta):
	if not animated_sprite:
		return

	var camera := get_viewport().get_camera()
	if not camera:
		return

	var camera_origin := camera.global_transform.origin
	var camera_position := Vector2(camera_origin.x, camera_origin.z)
	var position := Vector2(translation.x, translation.z)

	var angle := rad2deg((camera_position - position).rotated(rotation.y).angle())
	if angle < -135 or angle > 135:
		animated_sprite.animation = "facing_away"
	elif angle > 45:
		animated_sprite.animation = "facing_right"
	elif angle < -45:
		animated_sprite.animation = "facing_left"
	else:
		animated_sprite.animation = "facing_camera"
