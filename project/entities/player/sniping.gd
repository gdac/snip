extends "res://classes/state/state.gd"


var angle := 0.0
var drift_distance := 1.0
var drift_speed := 2.5


func enter():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	host.camera.fov = 30
	host.get_node("Vignette").visible = true


func exit():
	host.camera.fov = 85
	host.get_node("Vignette").visible = false
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func on_input(event: InputEvent):
	if event is InputEventMouseMotion:
		host.rotate_camera(event.relative)

func on_physics_process(delta):
	angle += delta * drift_speed

	host.rotate_camera(Vector2(sin(angle), cos(angle * 2)) * drift_distance)

	if Input.is_action_just_pressed("zoom"):
		state_machine.pop()
