class_name Player
extends KinematicBody
# The player character.


var pitch := 0.0
var view_sensitivity := 0.1

onready var camera: Camera = $Camera
onready var yaw := rad2deg(rotation.y)


func rotate_camera(intent: Vector2):
	yaw = fmod(yaw - intent.x * view_sensitivity, 360)
	pitch = max(min(pitch - intent.y * view_sensitivity, 85), -85)
	set_rotation(Vector3(0, deg2rad(yaw), 0))
	camera.set_rotation(Vector3(deg2rad(pitch), 0, 0))
